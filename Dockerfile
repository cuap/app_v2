# Usiamo una distro specifica
FROM python:3.11-alpine

# Copiamo e installiamo le dipendenze della nostra app con pip
COPY requirements.txt /app/
RUN pip install --no-cache-dir -r /app/requirements.txt

# Copiamo la nostra app
COPY src /app/

# Comunichiamo su che porta la nostra app e in ascolto
EXPOSE 5000

# Avvia l'app
CMD ["python", "/app/app.py"]
